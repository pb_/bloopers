class CreateGenres < ActiveRecord::Migration
  def change
    create_table :genres do |t|
      t.integer :imdb_id
      t.string :name
      t.boolean :status

      t.timestamps null: false
    end
  end
end
