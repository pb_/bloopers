class CreateActors < ActiveRecord::Migration
  def change
    create_table :actors do |t|
      t.integer :tmdb_id
      t.string :name
      t.text :description
      t.string :image
      t.boolean :status

      t.timestamps null: false
    end
  end
end
