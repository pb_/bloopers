class AddMovieTypeIndexToMovies < ActiveRecord::Migration
  def change
    add_index :movies, :movie_type_id
  end
end
