class CreateBloopers < ActiveRecord::Migration
  def change
    create_table :bloopers do |t|
      t.references :movie, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.string :video_link
      t.boolean :status

      t.timestamps null: false
    end
  end
end
