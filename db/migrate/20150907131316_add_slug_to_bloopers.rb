class AddSlugToBloopers < ActiveRecord::Migration
  def change
    add_column :bloopers, :slug, :string
    add_index :bloopers, :slug, unique: true
  end
end
