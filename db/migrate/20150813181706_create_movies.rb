class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.references :movie_type
      t.integer :tmdb_id
      t.string :imdb_id
      t.string :name
      t.string :description
      t.string :image
      t.date :release_date
      t.float :vote_average
      t.boolean :status

      t.timestamps null: false
    end
  end
end
