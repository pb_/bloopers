require 'rails_helper'

RSpec.describe "admin/genres/index", type: :view do
  before(:each) do
    assign(:admin_genres, Kaminari.paginate_array([
      Genre.create!(name: 'Test1', imdb_id: 123, status: true),
      Genre.create!(name: 'Test2', imdb_id: 223, status: true)
    ]).page(1))
  end

  it "renders a list of admin/genres" do
    render
  end
end
