require 'rails_helper'

RSpec.describe "admin/admins/new", type: :view do
  before(:each) do
    assign(:admin_admin, Admin.new({email: 'janek@mailinator.com', password: 'test000', password_confirmation: 'test000'}))
  end

  it "renders new admin_admin form" do
    render

    assert_select "form[action=?][method=?]", admin_admins_path, "post" do
    end
  end
end
