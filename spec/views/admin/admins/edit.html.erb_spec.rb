require 'rails_helper'

RSpec.describe "admin/admins/edit", type: :view do
  before(:each) do
    @admin_admin = assign(:admin_admin, Admin.create({email: 'janek@mailinator.com', password: 'test000', password_confirmation: 'test000'}))
  end

  it "renders the edit admin_admin form" do
    render

    assert_select "form[action=?][method=?]", admin_admin_path(@admin_admin), "post" do
    end
  end
end
