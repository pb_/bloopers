require 'rails_helper'

RSpec.describe "admin/admins/index", type: :view do
  before(:each) do
    assign(:admin_admins, Kaminari.paginate_array([
      Admin.create!({email: 'janek1@mailinator.com', password: 'test000', password_confirmation: 'test000'}),
      Admin.create!({email: 'janek2@mailinator.com', password: 'test000', password_confirmation: 'test000'})
    ]).page(1))
  end

  it "renders a list of admin/admins" do
    render
  end
end
