require 'rails_helper'

RSpec.describe "admin/movies/new", type: :view do
  before(:each) do
    assign(:admin_movie, build(:movie))
  end

  it "renders new admin_movie form" do
    render

    assert_select "form[action=?][method=?]", admin_movies_path, "post" do
    end
  end
end
