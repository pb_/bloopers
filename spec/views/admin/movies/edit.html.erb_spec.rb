require 'rails_helper'

RSpec.describe "admin/movies/edit", type: :view do
  before(:each) do
    @admin_movie = assign(:admin_movie, create(:movie))
  end

  it "renders the edit admin_movie form" do
    render

    assert_select "form[action=?][method=?]", admin_movie_path(@admin_movie), "post" do
    end
  end
end
