require 'rails_helper'

RSpec.describe "admin/bloopers/new", type: :view do
  before(:each) do
    assign(:admin_blooper, build(:blooper))
  end

  it "renders new admin_blooper form" do
    render

    assert_select "form[action=?][method=?]", admin_bloopers_path, "post" do
    end
  end
end
