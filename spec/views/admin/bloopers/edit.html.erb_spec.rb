require 'rails_helper'

RSpec.describe "admin/bloopers/edit", type: :view do
  before(:each) do
    @admin_blooper = assign(:admin_blooper, create(:blooper))
  end

  it "renders the edit admin_blooper form" do
    render

    assert_select "form[action=?][method=?]", admin_blooper_path(@admin_blooper), "post" do
    end
  end
end
