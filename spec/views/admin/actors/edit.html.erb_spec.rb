require 'rails_helper'

RSpec.describe "admin/actors/edit", type: :view do
  before(:each) do
    @admin_actor = assign(:admin_actor, create(:actor))
  end

  it "renders the edit admin_actor form" do
    render

    assert_select "form[action=?][method=?]", admin_actor_path(@admin_actor), "post" do
    end
  end
end
