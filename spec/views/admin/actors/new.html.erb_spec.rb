require 'rails_helper'

RSpec.describe "admin/actors/new", type: :view do
  before(:each) do
    #assign(:admin_actor, Admin::Actor.new())
    assign(:admin_actor, build(:actor))

  end

  it "renders new admin_actor form" do
    render

    assert_select "form[action=?][method=?]", admin_actors_path, "post" do
    end
  end
end
