require 'rails_helper'

RSpec.describe "admin/movie_types/new", type: :view do
  before(:each) do
    assign(:admin_movie_type, MovieType.new())
  end

  it "renders new admin_movie_type form" do
    render

    assert_select "form[action=?][method=?]", admin_movie_types_path, "post" do
    end
  end
end
