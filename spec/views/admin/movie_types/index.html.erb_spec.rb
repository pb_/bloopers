require 'rails_helper'

RSpec.describe "admin/movie_types/index", type: :view do
  before(:each) do
    assign(:admin_movie_types, [
      MovieType.create!(name: 'Movie', status: true),
      MovieType.create!(name: 'TV', status: true)
    ])
  end

  it "renders a list of admin/movie_types" do
    render
  end
end
