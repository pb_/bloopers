require 'rails_helper'

RSpec.describe "admin/movie_types/edit", type: :view do
  before(:each) do
    @admin_movie_type = assign(:admin_movie_type, MovieType.create!(name: 'Movie', status: true))
  end

  it "renders the edit admin_movie_type form" do
    render

    assert_select "form[action=?][method=?]", admin_movie_type_path(@admin_movie_type), "post" do
    end
  end
end
