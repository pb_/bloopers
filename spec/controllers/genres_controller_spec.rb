require 'rails_helper'

RSpec.describe GenresController, type: :controller do
  let(:valid_attributes) {
    attributes_for(:genre)
  }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      genre = Genre.create! valid_attributes
      get :show, {:id => genre.to_param}
      expect(response).to have_http_status(:success)
    end
  end

end
