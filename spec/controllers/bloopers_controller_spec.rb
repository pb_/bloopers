require 'rails_helper'

RSpec.describe BloopersController, type: :controller do
  let(:valid_attributes) {
    attributes_for(:blooper)
  }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      blooper = Blooper.create! valid_attributes
      get :show, {:id => blooper.to_param}
      expect(response).to have_http_status(:success)
    end
  end

end
