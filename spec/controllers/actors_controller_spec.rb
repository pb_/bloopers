require 'rails_helper'

RSpec.describe ActorsController, type: :controller do
  let(:valid_attributes) {
    attributes_for(:actor)
  }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      actor = Actor.create! valid_attributes
      get :show, {:id => actor.to_param}
      expect(response).to have_http_status(:success)
    end
  end

end
