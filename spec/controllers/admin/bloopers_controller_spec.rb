require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe Admin::BloopersController, type: :controller do
  login_admin

  # This should return the minimal set of attributes required to create a valid
  # Blooper. As you add validations to Blooper, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    attributes_for(:blooper)
  }

  let(:invalid_attributes) {
    attributes_for(:blooper, name: nil)
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # Admin::BloopersController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all admin_bloopers as @admin_bloopers" do
      blooper = Blooper.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:admin_bloopers)).to eq([blooper])
    end
  end

  describe "GET #show" do
    it "assigns the requested admin_blooper as @admin_blooper" do
      blooper = Blooper.create! valid_attributes
      get :show, {:id => blooper.to_param}, valid_session
      expect(assigns(:admin_blooper)).to eq(blooper)
    end
  end

  describe "GET #new" do
    it "assigns a new admin_blooper as @admin_blooper" do
      get :new, {}, valid_session
      expect(assigns(:admin_blooper)).to be_a_new(Blooper)
    end
  end

  describe "GET #edit" do
    it "assigns the requested admin_blooper as @admin_blooper" do
      blooper = Blooper.create! valid_attributes
      get :edit, {:id => blooper.to_param}, valid_session
      # expect(assigns(:admin_blooper)).to eq(blooper)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Blooper" do
        # expect {
        #   post :create, {:blooper => valid_attributes}, valid_session
        # }.to change(Blooper, :count).by(1)
      end

      it "assigns a newly created admin_blooper as @admin_blooper" do
        post :create, {:blooper => valid_attributes}, valid_session
        expect(assigns(:admin_blooper)).to be_a(Blooper)
        # expect(assigns(:admin_blooper)).to be_persisted
      end

      it "redirects to the created admin_blooper" do
        post :create, {:blooper => valid_attributes}, valid_session
        # expect(response).to redirect_to(Blooper.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved admin_blooper as @admin_blooper" do
        post :create, {:blooper => invalid_attributes}, valid_session
        expect(assigns(:admin_blooper)).to be_a_new(Blooper)
      end

      it "re-renders the 'new' template" do
        post :create, {:blooper => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        attributes_for(:blooper)
      }

      it "updates the requested admin_blooper" do
        blooper = Blooper.create! valid_attributes
        put :update, {:id => blooper.to_param, :blooper => new_attributes}, valid_session
        blooper.reload
        expect(assigns(:admin_blooper)).to be_a(Blooper)
      end

      it "assigns the requested admin_blooper as @admin_blooper" do
        blooper = Blooper.create! valid_attributes
        put :update, {:id => blooper.to_param, :blooper => valid_attributes}, valid_session
        expect(assigns(:admin_blooper)).to eq(blooper)
      end

      it "redirects to the admin_blooper" do
        blooper = Blooper.create! valid_attributes
        put :update, {:id => blooper.to_param, :blooper => valid_attributes}, valid_session
        expect(response).to redirect_to([:admin, blooper])
      end
    end

    context "with invalid params" do
      it "assigns the admin_blooper as @admin_blooper" do
        blooper = Blooper.create! valid_attributes
        put :update, {:id => blooper.to_param, :blooper => invalid_attributes}, valid_session
        expect(assigns(:admin_blooper)).to eq(blooper)
      end

      it "re-renders the 'edit' template" do
        blooper = Blooper.create! valid_attributes
        put :update, {:id => blooper.to_param, :blooper => invalid_attributes}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested admin_blooper" do
      blooper = Blooper.create! valid_attributes
      expect {
        delete :destroy, {:id => blooper.to_param}, valid_session
      }.to change(Blooper, :count).by(-1)
    end

    it "redirects to the admin_bloopers list" do
      blooper = Blooper.create! valid_attributes
      delete :destroy, {:id => blooper.to_param}, valid_session
      expect(response).to redirect_to(admin_bloopers_url)
    end
  end

end
