require 'rails_helper'

RSpec.describe MoviesController, type: :controller do
  let(:valid_attributes) {
    attributes_for(:movie)
  }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      movie = Movie.create! valid_attributes
      get :show, {:id => movie.to_param}
      expect(response).to have_http_status(:success)
    end
  end

end
