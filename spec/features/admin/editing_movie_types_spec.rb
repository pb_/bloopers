require 'rails_helper'

feature 'Editing Movie Types' do
  let(:admin) { create(:admin) }
  let!(:movie_type) { create(:movie_type) }

  before { sign_in_as admin }

  scenario 'successfully editing an movie type' do
    visit admin_path
    click_link 'Movie Types' , match: :first
    click_link 'Edit'

    fill_in 'Name', with: 'Movie 2'
    check 'movie_type_status'
    click_button 'Submit'

    expect(page).to have_content('Name: Movie 2')
    expect(page).to have_content('Active: true')
    expect(movie_type.reload.name).to eq('Movie 2')
  end

end