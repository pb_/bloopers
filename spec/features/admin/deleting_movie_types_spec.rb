require 'rails_helper'

feature 'Deleting Movie Types' do
  let(:admin) { create(:admin) }
  let!(:movie_type) { create(:movie_type) }

  before { sign_in_as admin }

  scenario 'successfully deleting an movie type' do
    visit admin_path
    click_link 'Movie Types' , match: :first
    click_link 'Delete'

    expect(page).to have_content('Listing Movie Types')
    expect(page.current_path).to eq(admin_movie_types_path)
  end

end