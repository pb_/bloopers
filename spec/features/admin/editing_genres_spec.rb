require 'rails_helper'

feature 'Editing Genres' do
  let(:admin) { create(:admin) }
  let!(:genre) { create(:genre) }

  before { sign_in_as admin }

  scenario 'successfully editing a genre' do
    visit admin_path
    click_link 'Genres' , match: :first
    click_link 'Edit'

    fill_in 'Name', with: 'Action 2'
    click_button 'Submit'

    expect(page).to have_content('Name: Action 2')
    expect(genre.reload.name).to eq('Action 2')
  end

end