require 'rails_helper'

feature 'Deleting Genres' do
  let(:admin) { create(:admin) }
  let!(:genre) { create(:genre) }

  before { sign_in_as admin }

  scenario 'successfully deleting a genre' do
    visit admin_path
    click_link 'Genres' , match: :first
    click_link 'Delete'

    expect(page).to have_content('Listing Genres')
    expect(page.current_path).to eq(admin_genres_path)
  end

end