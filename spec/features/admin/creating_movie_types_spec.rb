require 'rails_helper'

feature 'Creating Movie Types' do
  let(:admin) { create(:admin) }

  before { sign_in_as admin }

  scenario 'create a valid movie type' do
    visit admin_path
    click_link 'Movie Types' , match: :first
    click_link 'add'

    fill_in 'Name', with: 'Movie'
    check 'movie_type_status'
    click_button 'Submit'

    expect(page).to have_content('Name: Movie')
    expect(page).to have_content('Active: true')
  end

  scenario 'create a invalid movie type' do
    visit admin_path
    click_link 'Movie Types' , match: :first
    click_link 'add'

    fill_in 'Name', with: ''
    check 'movie_type_status'
    click_button 'Submit'

    expect(page).to have_content('New Movie Type')
  end
end