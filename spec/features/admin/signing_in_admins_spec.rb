require 'rails_helper'

feature 'Signing In Admins' do
  let!(:admin) { create(:admin, email: 'test@mailinator.com') }

  scenario 'successful signin' do
    visit admin_path

    fill_in 'admin_email', with: admin.email
    fill_in 'admin_password', with: admin.password
    click_button 'Submit'

    expect(page).to have_content('Dashboard')
  end

  scenario 'unsuccessful signin' do
    visit admin_path

    fill_in 'admin_email', with: admin.email
    fill_in 'admin_password', with: ''
    click_button 'Submit'

    expect(page).to have_content('Log in')
  end

end