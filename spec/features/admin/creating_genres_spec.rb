require 'rails_helper'

feature 'Creating Genres' do
  let(:admin) { create(:admin) }
  let(:genre) { attributes_for(:genre) }

  before { sign_in_as admin }

  scenario 'create a valid genre' do
    visit admin_path
    click_link 'Genres' , match: :first
    click_link 'add'

    fill_in 'Name', with: genre[:name]
    fill_in 'Imdb', with: genre[:imdb_id]
    check 'genre_status'
    click_button 'Submit'

    expect(page).to have_content("Name: #{genre[:name]}")
    expect(page).to have_content("Imdb id: #{genre[:imdb_id]}")
    expect(page).to have_content('Active: true')
    expect(page.current_path).to eq(admin_genre_path(Genre.first.slug))
  end

  scenario 'create a invalid movei type' do
    visit admin_path
    click_link 'Genres' , match: :first
    click_link 'add'

    fill_in 'Name', with: ''
    fill_in 'Imdb', with: genre[:imdb_id]
    check 'genre_status'
    click_button 'Submit'

    expect(page.current_path).to eq(admin_genres_path)
  end

  scenario 'create a valid genre' do
    visit admin_path
    click_link 'Genres' , match: :first
    click_link 'import_export'

    expect(page).to have_content('Action')
    expect(page).to have_content('Adventure')
    expect(page).to have_content('Crime')
    expect(page.current_path).to eq(admin_genres_path)
  end
end