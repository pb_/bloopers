require 'rails_helper'

feature 'Creating Genres' do
  let(:admin) { create(:admin) }

  before { sign_in_as admin }

  scenario 'create a valid actor fetched from tmdb', js: true do
    visit admin_path
    click_link 'Actors' , match: :first
    click_link 'add'

    fill_in 'search', with: 'Angelina'
    find('#btn-search').click
    wait_for_ajax
    find("div[data-id='11701']").click
    wait_for_ajax

    click_button 'Submit'

    expect(page.current_path).to eq(admin_actor_path(Actor.first.slug))
  end

end