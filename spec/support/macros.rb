def sign_in_as(admin)
  visit admin_path

  fill_in 'admin_email', with: admin.email
  fill_in 'admin_password', with: admin.password
  click_button 'Submit'

  expect(page).to have_content('Dashboard')
end