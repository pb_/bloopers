FactoryGirl.define do
  factory :admin do
    email FFaker::Internet.email
    password 'password'
    password_confirmation 'password'
  end

end
