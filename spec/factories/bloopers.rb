FactoryGirl.define do
  factory :blooper do
    movie {FactoryGirl.create(:movie)}
    name FFaker::Name.name
    description FFaker::Lorem.paragraph
    video_link 'wjFP2qRwIaQ'
    status true
  end

end
