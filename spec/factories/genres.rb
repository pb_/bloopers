FactoryGirl.define do
  factory :genre do
    imdb_id 28
    name "Action"
    status true
  end

  factory :comedy do
    imdb_id 35
    name "Comedy"
    status true
  end

end
