FactoryGirl.define do
  factory :actor do
    tmdb_id 2887
    name FFaker::Name.name
    description FFaker::Lorem.paragraph
    image "/w8zJQuN7tzlm6FY9mfGKihxp3Cb.jpg"
    status true
  end

end
