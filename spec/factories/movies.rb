FactoryGirl.define do
  factory :movie do
    movie_type {FactoryGirl.create(:movie_type)}
    genres {[FactoryGirl.create(:genre)]}
    actors {[FactoryGirl.create(:actor)]}
    tmdb_id "550"
    imdb_id "tt0137523"
    name "Fight Club"
    description FFaker::Lorem.paragraph
    image "/2lECpi35Hnbpa4y46JX0aY3AWTy.jpg"
    release_date FFaker::Time.date
    vote_average '7'
    status true
  end

end
