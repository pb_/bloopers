FactoryGirl.define do
  factory :movie_type do
    name "Movie"
    status true
    factory :type_with_movie do
      after(:create) do |type|
        create_list(:movie, movie_type: type)
      end
    end
    trait :tv do
      name  'TV'
      status true
    end
  end

end
