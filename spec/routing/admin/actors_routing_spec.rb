require "rails_helper"

RSpec.describe Admin::ActorsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/actors").to route_to("admin/actors#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/actors/new").to route_to("admin/actors#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/actors/1").to route_to("admin/actors#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/actors/1/edit").to route_to("admin/actors#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/actors").to route_to("admin/actors#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/actors/1").to route_to("admin/actors#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/actors/1").to route_to("admin/actors#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/actors/1").to route_to("admin/actors#destroy", :id => "1")
    end

  end
end
