require "rails_helper"

RSpec.describe Admin::BloopersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/bloopers").to route_to("admin/bloopers#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/bloopers/new").to route_to("admin/bloopers#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/bloopers/1").to route_to("admin/bloopers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/bloopers/1/edit").to route_to("admin/bloopers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/bloopers").to route_to("admin/bloopers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/bloopers/1").to route_to("admin/bloopers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/bloopers/1").to route_to("admin/bloopers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/bloopers/1").to route_to("admin/bloopers#destroy", :id => "1")
    end

  end
end
