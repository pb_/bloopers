require 'rails_helper'

RSpec.describe MovieType, :type => :model do
  it { should have_many(:movies) }

  it 'is a valid with name and status' do
    expect(build(:movie_type)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:movie_type, name: nil)).to_not be_valid
  end

  it 'is invalid with a name length longer than 256' do
    expect(build(:movie_type, name: 'abc'*1000)).to_not be_valid
  end

  it 'is valid without a status' do
    expect(build(:movie_type, status: nil)).to be_valid
  end
end