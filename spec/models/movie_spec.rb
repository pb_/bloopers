require 'rails_helper'

RSpec.describe Movie, type: :model do

  it { should belong_to(:movie_type) }
  it { should have_many(:genres).through(:movie_genres)  }
  it { should have_many(:actors).through(:movie_actors)  }

  it 'is a valid' do
    expect(build(:movie)).to be_valid
  end

  it 'is invalid without a tmdb_id' do
    expect(build(:movie, tmdb_id: nil)).to_not be_valid
  end

  it 'is invalid without a name' do
    expect(build(:movie, name: nil)).to_not be_valid
  end

  it 'is invalid without a type' do
    expect(build(:movie, movie_type: nil)).to_not be_valid
  end

  it 'is invalid without a description' do
    expect(build(:movie, description: nil)).to_not be_valid
  end

  it 'is invalid with a name length longer than 256' do
    expect(build(:movie, name: 'abc'*1000)).to_not be_valid
  end

  it 'is invalid with a image length longer than 256' do
    expect(build(:movie, image: 'abc'*1000)).to_not be_valid
  end

  it 'is invalid with a imdb_id length longer than 256' do
    expect(build(:movie, imdb_id: 'abc'*1000)).to_not be_valid
  end

  it 'is invalid with a tmdb_id length longer than 256' do
    expect(build(:movie, tmdb_id: 'abc'*1000)).to_not be_valid
  end
end
