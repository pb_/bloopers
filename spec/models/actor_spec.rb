require 'rails_helper'

RSpec.describe Actor, :type => :model do
  it 'is a valid' do
    expect(build(:actor)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:actor, name: nil)).to_not be_valid
  end

  it 'is invalid with a name length longer than 256' do
    expect(build(:actor, name: 'abc'*1000)).to_not be_valid
  end

  it 'is valid without a status' do
    expect(build(:actor, status: nil)).to be_valid
  end

  it 'is invalid without a tmdb_id' do
    expect(build(:actor, tmdb_id: nil)).to_not be_valid
  end

  it 'is invalid with a tmdb_id string' do
    expect(build(:actor, tmdb_id: "test")).to_not be_valid
  end

  it 'is invalid without a image' do
    expect(build(:actor, image: nil)).to_not be_valid
  end

  it 'is invalid without a description' do
    expect(build(:actor, description: nil)).to_not be_valid
  end
end