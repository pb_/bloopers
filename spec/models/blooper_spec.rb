require 'rails_helper'

RSpec.describe Blooper, type: :model do

  it { should belong_to(:movie) }

  it 'is a valid' do
    expect(build(:blooper)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:blooper, name: nil)).to_not be_valid
  end

  it 'is invalid without a movie' do
    expect(build(:blooper, movie: nil)).to_not be_valid
  end

  it 'is invalid without a video link' do
    expect(build(:blooper, video_link: nil)).to_not be_valid
  end

  it 'is valid without a description' do
    expect(build(:blooper, description: nil)).to be_valid
  end

  it 'is invalid with a name length longer than 256' do
    expect(build(:blooper, name: 'abc'*1000)).to_not be_valid
  end

  it 'is invalid with a video link length longer than 256' do
    expect(build(:blooper, video_link: 'abc'*1000)).to_not be_valid
  end

end
