require 'rails_helper'

RSpec.describe Genre, :type => :model do
  it { should have_many(:movie_genres) }
  it { should have_many(:movies).through(:movie_genres) }

  it 'is a valid with name and status' do
    expect(build(:genre)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:genre, name: nil)).to_not be_valid
  end

  it 'is invalid with a name length longer than 256' do
    expect(build(:genre, name: 'abc'*1000)).to_not be_valid
  end

  it 'is valid without a status' do
    expect(build(:genre, status: nil)).to be_valid
  end

  it 'is invalid without a imdb_id' do
    expect(build(:genre, imdb_id: nil)).to_not be_valid
  end

  it 'is invalid with a imdb_id string' do
    expect(build(:genre, imdb_id: "test")).to_not be_valid
  end
end