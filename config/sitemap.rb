# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.bloopers.rocks"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  add bloopers_path, :priority => 0.7, :changefreq => 'daily'
  Blooper.find_each do |blooper|
    add blooper_path(blooper), :lastmod => blooper.updated_at if blooper.status
  end
  add movies_path, :priority => 0.7, :changefreq => 'daily'
  Movie.find_each do |movie|
    add movie_path(movie), :lastmod => movie.updated_at if movie.status and movie.movie_type.name == 'Movie'
  end
  add tvs_path, :priority => 0.7, :changefreq => 'daily'
  Movie.find_each do |tv|
    add tv_path(tv), :lastmod => tv.updated_at if tv.status and tv.movie_type.name == 'TV'
  end
  add actors_path, :priority => 0.7, :changefreq => 'daily'
  Actor.find_each do |actor|
    add actor_path(actor), :lastmod => actor.updated_at if actor.status
  end
  add genres_path, :priority => 0.7, :changefreq => 'daily'
  Genre.find_each do |genre|
    add genre_path(genre), :lastmod => genre.updated_at if genre.status
  end
  add pages_about_path, :priority => 0.5, :changefreq => 'daily'
  add pages_contact_path, :priority => 0.5, :changefreq => 'daily'
end
