Rails.application.routes.draw do

  get 'pages/about'
  get 'pages/contact'

  devise_for :admins
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root to: "bloopers#index"
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  concern :paginatable do
    get '(page/:page)', :action => :index, :on => :collection, :as => ''
  end

  # get '/' => 'bloopers#index', as: :bloopers
  post 'search', to: 'search#search'
  resources :bloopers, :actors, :movies, :tvs, :genres, only: [:index, :show], :concerns => :paginatable


  namespace :admin do
    get '/' => '/admin/base#index'

    namespace :tmdb do
      get 'genres' => '/admin/tmdb#genres_list'
      get 'import_genres' => '/admin/tmdb#import_genres'
      post 'person_search' => '/admin/tmdb#person_search'
      post 'person' => '/admin/tmdb#person'
      post 'movie_search' => '/admin/tmdb#movie_search'
      post 'movie' => '/admin/tmdb#movie'
      post 'tv' => '/admin/tmdb#tv'
      post 'add_person' => '/admin/tmdb#add_person'
    end

    resources :movie_types, :genres, :actors, :movies, :bloopers, :admins, :visits, :concerns => :paginatable
  end

end
