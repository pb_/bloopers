namespace :elasticsearch do
  desc "Movie"
  task movie: :environment do
    # Delete the previous articles index in Elasticsearch
    Movie.__elasticsearch__.client.indices.delete index: Movie.index_name rescue nil

    # Create the new index with the new mapping
    Movie.__elasticsearch__.client.indices.create \
      index: Movie.index_name,
      body: { settings: Movie.settings.to_hash, mappings: Movie.mappings.to_hash }

    # Index all article records from the DB to Elasticsearch
    Movie.import
  end

  desc "Actor"
  task actor: :environment do
    # Delete the previous articles index in Elasticsearch
    Actor.__elasticsearch__.client.indices.delete index: Actor.index_name rescue nil

    # Create the new index with the new mapping
    Actor.__elasticsearch__.client.indices.create \
      index: Actor.index_name,
      body: { settings: Actor.settings.to_hash, mappings: Actor.mappings.to_hash }

  # Index all article records from the DB to Elasticsearch
    Actor.import
  end

  desc 'Run all'
  task :runall => [:movie, :actor ] do
    # This will run after all those tasks have run
  end

end
