json.array!(@admin_movies) do |admin_movie|
  json.extract! admin_movie, :id
  json.url admin_movie_url(admin_movie, format: :json)
end
