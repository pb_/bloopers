json.array!(@admin_bloopers) do |admin_blooper|
  json.extract! admin_blooper, :id
  json.url admin_blooper_url(admin_blooper, format: :json)
end
