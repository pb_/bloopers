json.array!(@admin_actors) do |admin_actor|
  json.extract! admin_actor, :id
  json.url admin_actor_url(admin_actor, format: :json)
end
