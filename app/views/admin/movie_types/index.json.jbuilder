json.array!(@admin_movie_types) do |admin_movie_type|
  json.extract! admin_movie_type, :id
  json.url admin_movie_type_url(admin_movie_type, format: :json)
end
