class Admin::BloopersController < Admin::BaseController
  before_action :set_admin_blooper, only: [:show, :edit, :update, :destroy]

  # GET /admin/bloopers
  # GET /admin/bloopers.json
  def index
    @admin_bloopers = Blooper.includes(:movie).order(created_at: :desc).page(params[:page]).per(20)
  end

  # GET /admin/bloopers/1
  # GET /admin/bloopers/1.json
  def show
  end

  # GET /admin/bloopers/new
  def new
    @admin_blooper = Blooper.new
  end

  # GET /admin/bloopers/1/edit
  def edit
  end

  # POST /admin/bloopers
  # POST /admin/bloopers.json
  def create
    @admin_blooper = Blooper.new(admin_blooper_params)

    respond_to do |format|
      if @admin_blooper.save
        format.html { redirect_to [:admin, @admin_blooper], notice: 'Blooper was successfully created.' }
        format.json { render :show, status: :created, location: @admin_blooper }
      else
        format.html { render :new }
        format.json { render json: @admin_blooper.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/bloopers/1
  # PATCH/PUT /admin/bloopers/1.json
  def update
    respond_to do |format|
      if @admin_blooper.update(admin_blooper_params)
        format.html { redirect_to [:admin, @admin_blooper], notice: 'Blooper was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_blooper }
      else
        format.html { render :edit }
        format.json { render json: @admin_blooper.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/bloopers/1
  # DELETE /admin/bloopers/1.json
  def destroy
    @admin_blooper.destroy
    respond_to do |format|
      format.html { redirect_to admin_bloopers_url, notice: 'Blooper was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_blooper
      @admin_blooper = Blooper.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_blooper_params
      params.require(:blooper).permit(:name, :video_link, :movie_id, :description, :status)

    end
end
