class Admin::MovieTypesController < Admin::BaseController
  before_action :set_admin_movie_type, only: [:show, :edit, :update, :destroy]

  # GET /admin/movie_types
  # GET /admin/movie_types.json
  def index
    @admin_movie_types = MovieType.all
  end

  # GET /admin/movie_types/1
  # GET /admin/movie_types/1.json
  def show
  end

  # GET /admin/movie_types/new
  def new
    @admin_movie_type = MovieType.new
  end

  # GET /admin/movie_types/1/edit
  def edit
  end

  # POST /admin/movie_types
  # POST /admin/movie_types.json
  def create
    @admin_movie_type = MovieType.new(admin_movie_type_params)

    respond_to do |format|
      if @admin_movie_type.save
        format.html { redirect_to [:admin, @admin_movie_type], notice: 'Movie type was successfully created.' }
        format.json { render :show, status: :created, location: @admin_movie_type }
      else
        format.html { render :new }
        format.json { render json: @admin_movie_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/movie_types/1
  # PATCH/PUT /admin/movie_types/1.json
  def update
    respond_to do |format|
      if @admin_movie_type.update(admin_movie_type_params)
        format.html { redirect_to [:admin, @admin_movie_type], notice: 'Movie type was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_movie_type }
      else
        format.html { render :edit }
        format.json { render json: @admin_movie_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/movie_types/1
  # DELETE /admin/movie_types/1.json
  def destroy
    @admin_movie_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_movie_types_url, notice: 'Movie type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_movie_type
      @admin_movie_type = MovieType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_movie_type_params
      params.require(:movie_type).permit(:name, :status)
    end
end
