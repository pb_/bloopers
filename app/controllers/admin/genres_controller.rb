class Admin::GenresController < Admin::BaseController
  before_action :set_admin_genre, only: [:show, :edit, :update, :destroy]

  # GET /admin/genres
  # GET /admin/genres.json
  def index
    @admin_genres = Genre.order(:name).page(params[:page]).per(10)
  end

  # GET /admin/genres/1
  # GET /admin/genres/1.json
  def show
  end

  # GET /admin/genres/new
  def new
    @admin_genre = Genre.new
  end

  # GET /admin/genres/1/edit
  def edit
  end

  # POST /admin/genres
  # POST /admin/genres.json
  def create
    @admin_genre = Genre.new(admin_genre_params)

    respond_to do |format|
      if @admin_genre.save
        format.html { redirect_to [:admin, @admin_genre], notice: 'Genre was successfully created.' }
        format.json { render :show, status: :created, location: @admin_genre }
      else
        format.html { render :new }
        format.json { render json: @admin_genre.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/genres/1
  # PATCH/PUT /admin/genres/1.json
  def update
    respond_to do |format|
      if @admin_genre.update(admin_genre_params)
        format.html { redirect_to [:admin, @admin_genre], notice: 'Genre was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_genre }
      else
        format.html { render :edit }
        format.json { render json: @admin_genre.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/genres/1
  # DELETE /admin/genres/1.json
  def destroy
    @admin_genre.destroy
    respond_to do |format|
      format.html { redirect_to admin_genres_url, notice: 'Genre was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_genre
      @admin_genre = Genre.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_genre_params
      params.require(:genre).permit(:name, :imdb_id, :status)

    end
end
