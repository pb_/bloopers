class Admin::ActorsController < Admin::BaseController
  before_action :set_admin_actor, only: [:show, :edit, :update, :destroy]

  # GET /admin/actors
  # GET /admin/actors.json
  def index
    # @admin_actors = Actor.all
    @admin_actors = Actor.order(:name).page(params[:page]).per(10)
  end

  # GET /admin/actors/1
  # GET /admin/actors/1.json
  def show
  end

  # GET /admin/actors/new
  def new
    @admin_actor = Actor.new
  end

  # GET /admin/actors/1/edit
  def edit
  end

  # POST /admin/actors
  # POST /admin/actors.json
  def create
    @admin_actor = Actor.new(admin_actor_params)

    respond_to do |format|
      if @admin_actor.save
        format.html { redirect_to [:admin, @admin_actor], notice: 'Actor was successfully created.' }
        format.json { render :show, status: :created, location: @admin_actor }
      else
        format.html { render :new }
        format.json { render json: @admin_actor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/actors/1
  # PATCH/PUT /admin/actors/1.json
  def update
    respond_to do |format|
      if @admin_actor.update(admin_actor_params)
        format.html { redirect_to [:admin, @admin_actor], notice: 'Actor was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_actor }
      else
        format.html { render :edit }
        format.json { render json: @admin_actor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/actors/1
  # DELETE /admin/actors/1.json
  def destroy
    @admin_actor.destroy
    respond_to do |format|
      format.html { redirect_to admin_actors_url, notice: 'Actor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_actor
      @admin_actor = Actor.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_actor_params
      params.require(:actor).permit(:name, :tmdb_id, :image, :description, :status)
    end
end
