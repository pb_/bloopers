class Admin::TmdbController < Admin::BaseController

  before_filter :set_config
  Tmdb::Api.key(ENV['tmdb_key'])

  def set_config
    @configuration = Tmdb::Configuration.new
  end

  def genres_list
    genre = Tmdb::Genre.list
    respond_to do |format|
      format.json { render json: genre }
    end
  end

  # Import genres from TMDB
  def import_genres
    genres = Tmdb::Genre.list
    if !genres['genres'].nil?
      Genre.import genres['genres']
    end
    redirect_to admin_genres_url, notice: 'Genres was successfully imported'
  end

  def person_search
    #search = Tmdb::Search.new
    #search.resource('person') # determines type of resource
    #search.query('samuel jackson') # the query to search against

    persons = Tmdb::Person.find(params[:name])
    persons << {image_link: "#{@configuration.secure_base_url}w45"}
    respond_to do |format|
      format.json { render json: persons }
    end
  end

  def person
    person = Tmdb::Person.detail(params[:id])
    if person
      person['full_profile_path'] = "#{@configuration.secure_base_url}h632#{person['profile_path']}"
    end
    respond_to do |format|
      format.json { render json: person }
    end
  end

  def add_person
    person = Tmdb::Person.detail(params[:id])
    if person
      person['full_profile_path'] = "#{@configuration.secure_base_url}h632#{person['profile_path']}"
    end
    actor = Actor.find_by(tmdb_id: person['id'])
    if actor.nil?
      actor = Actor.create!(name: person['name'], tmdb_id: person['id'], image:person['full_profile_path'] , description: person['biography'], status: true)
    end
    respond_to do |format|
      format.json { render json: actor }
    end
  end

  def movie_search
    movies = {}
    movies['movies'] = Tmdb::Movie.find(params[:name])
    movies['tv'] = Tmdb::TV.find(params[:name])
    movies['image_link'] = "#{@configuration.secure_base_url}w92"
    respond_to do |format|
      format.json { render json: movies }
    end
  end

  def movie
    movie = Tmdb::Movie.detail(params[:id])
    movie['persons'] = Tmdb::Movie.casts(params[:id])
    movie['image_link'] = "#{@configuration.secure_base_url}w45"
    if movie
      movie['full_poster_path'] = "#{@configuration.secure_base_url}w780#{movie['poster_path']}"
    end
    respond_to do |format|
      format.json { render json: movie }
    end
  end

  def tv
    tv = Tmdb::TV.detail(params[:id])
    tv['persons'] = Tmdb::TV.cast(params[:id])
    tv['image_link'] = "#{@configuration.secure_base_url}w45"
    if tv
      tv['full_poster_path'] = "#{@configuration.secure_base_url}w780#{tv['poster_path']}"
    end
    respond_to do |format|
      format.json { render json: tv }
    end
  end
end
