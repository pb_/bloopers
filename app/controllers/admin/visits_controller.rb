class Admin::VisitsController < Admin::BaseController
  def index
    @admin_visits = Visit.order(started_at: :desc).page(params[:page]).per(25)
  end

  def show
    @admin_visit = Visit.find(params[:id])
  end
end
