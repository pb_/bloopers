class GenresController < ApplicationController
  include GenresHelper

  # GET /genres
  def index
    Rails.cache.write(cache_key_for_genres, Genre.where(status: true).order(name: :asc)) until Rails.cache.read(cache_key_for_genres)
    @genres = Rails.cache.read(cache_key_for_genres)
  end

  # GET /genres/1
  def show
    key = "genre-#{params[:id]}"
    Rails.cache.write(key, (Genre.includes(movies: [:movie_type, :bloopers]).friendly.where(slug: params[:id], status: :true).first  or not_found), expires_in: 2.hours) until Rails.cache.read(key)
    @genre = Rails.cache.read(key)

    ahoy.track 'Viewed genre', id: @genre.id, title: @genre.name
  end
end
