class MoviesController < ApplicationController
  include MoviesHelper
  # GET /movies
  def index
    until Rails.cache.read(cache_key_for_movies)
      movie = MovieType.find_by_name('Movie')
      Rails.cache.write(cache_key_for_movies, Movie.where(status: :true, movie_type: movie).order(created_at: :desc))
    end
    movies = Rails.cache.read(cache_key_for_movies)
    @movies = prepare_reverse_pagination movies, 9
  end

  # GET /movies/1
  def show
    key = "movie-#{params[:id]}"
    Rails.cache.write(key, (Movie.friendly.where(slug: params[:id], status: :true).first  or not_found), expires_in: 2.hours) until Rails.cache.read(key)
    @movie = Rails.cache.read(key)

    ahoy.track 'Viewed Movie', id: @movie.id, title: @movie.name

    set_meta_tags description: @movie.description,
                  og: {
                      title:    "Bloopers | #{@movie.name}",
                      type:     'website',
                      url:      movie_url(@movie),
                      image:    @movie.image
                  }
  end

end
