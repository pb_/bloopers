class ActorsController < ApplicationController
  include ActorsHelper
  # GET /actors
  def index
    Rails.cache.write(cache_key_for_actors, Actor.where(status: :true).order(created_at: :desc), expires_in: 2.hours) until Rails.cache.read(cache_key_for_actors)
    actors = Rails.cache.read(cache_key_for_actors)
    @actors = prepare_reverse_pagination actors, 9
  end

  # GET /actors/1
  def show
    key = "actor-#{params[:id]}"
    Rails.cache.write(key, (Actor.includes(movies: [:movie_type, :bloopers]).friendly.where(slug: params[:id], status: :true).first  or not_found), expires_in: 2.hours) until Rails.cache.read(key)
    @actor = Rails.cache.read(key)
    ahoy.track 'Viewed Actor', id: @actor.id, title: @actor.name

    set_meta_tags description: @actor.description,
                  og: {
                      title:    "Bloopers | #{@actor.name}",
                      type:     'website',
                      url:      actor_url(@actor),
                      image:    @actor.image
                  }
  end

end
