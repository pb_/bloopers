class BloopersController < ApplicationController
  include BloopersHelper
  # GET /bloopers
  def index
    Rails.cache.write(cache_key_for_bloopers, Blooper.includes(:movie).where(status: :true).order(created_at: :desc), expires_in: 1.hours) until Rails.cache.read(cache_key_for_bloopers)
    bloopers = Rails.cache.read(cache_key_for_bloopers)
    @bloopers = prepare_reverse_pagination bloopers
  end

  # GET /bloopers/1
  def show
    key = "blooper-#{params[:id]}"
    Rails.cache.write(key, (Blooper.friendly.where(slug: params[:id], status: :true).first or not_found), expires_in: 2.hours) until Rails.cache.read(key)
    @blooper = Rails.cache.read(key)

    ahoy.track 'Viewed blooper', id: @blooper.id, title: @blooper.name

    set_meta_tags description: @blooper.description,
                  og: {
                      title:    "Bloopers | #{@blooper.name}",
                      type:     'website',
                      url:      blooper_url(@blooper),
                      video:    "https://www.youtube.com/embed/#{@blooper.video_link}?rel=0"
                  }
  end

end
