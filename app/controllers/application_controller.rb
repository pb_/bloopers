class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :layout_by_resource
  before_action :set_mtags

  def not_found
    raise ActionController::RecordNotFound.new('Not Found')
  end

  protected

  def set_mtags
    set_meta_tags keywords: %w[Bloopers blooper gag\ reel gag outtakes],
                  description: 'A blooper, also known as an outtake or gag reel, is a short sequence of a film or
                               video production, usually a deleted scene, containing a mistake made by a member of
                               the cast or crew.',
                  canonical: root_url,
                  og: {
                    title:    'Bloopers',
                    type:     'website',
                    url:      root_url,
                    image:    root_url + ActionController::Base.helpers.asset_path('movie-clapper-board-152-130297.png')
                  }
  end

  def prepare_reverse_pagination(scope, per_page=10)
    @per_page = per_page
    total_count = scope.count
    rest_count = total_count > @per_page ? (total_count % @per_page) : 0
    @num_pages = total_count > @per_page ? (total_count / @per_page) : 1

    if params[:page]
      offset = params[:page].sub(/-.*/, '').to_i
      current_page = @num_pages - (offset - 1) / @per_page
      scope.page(current_page).per(@per_page).padding(rest_count)
    else
      scope.page(1).per(@per_page + rest_count)
    end
  end

  def layout_by_resource
    if devise_controller?
      'admin'
    else
      'application'
    end
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

end
