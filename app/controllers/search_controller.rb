class SearchController < ApplicationController

  def search
    if params[:q].nil?
      @search = []
    else
      @search = Elasticsearch::Model.search("#{params[:q]}*", [Movie, Actor]).results.to_a.map(&:to_hash)
      # @search = Actor.__elasticsearch__.client.suggest(:index => Actor.index_name, :body => {
      #        :suggestions => {
      #            :text => params[:q],
      #            :term => {
      #                :field => 'name'
      #            }
      #        }
      #    })
    end
    respond_to do |format|
      format.json { render json: @search }
    end
  end
end
