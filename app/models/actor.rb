class Actor < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :movie_actors, dependent: :destroy
  has_many :movies, through: :movie_actors

  validates :tmdb_id, :name, :description, :image, presence: true
  validates :tmdb_id, numericality: { only_integer: true }
  validates :name, :image, length: { maximum: 256 }

  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :name, analyzer: 'english'
      indexes :description, analyzer: 'english'
    end
  end

  def self.search(query)
    __elasticsearch__.search(
      {
        query: {
          multi_match: {
            query: query,
            fields: ['name^5', 'description']
          }
        }
      }
    )
  end

  def as_indexed_json(options = {})
    self.as_json({
         only: [:name, :description, :image, :slug]
     })
  end
end