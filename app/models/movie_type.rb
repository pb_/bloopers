class MovieType < ActiveRecord::Base
  has_many :movies
  validates :name, presence: true
  validates :name, length: { maximum: 256 }
end
