class Genre < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :movie_genres, dependent: :destroy
  has_many :movies, through: :movie_genres

  validates :imdb_id, :name, presence: true
  validates :imdb_id, numericality: { only_integer: true }, length: { maximum: 8 }
  validates :name, length: { maximum: 256 }

  # import genres
  # genres - array
  def self.import( genres )
    if !genres.nil?
      genres.each do |genre|
        g = self.find_by(name: genre['name'])
        if g.nil?
          self.create(name: genre['name'], imdb_id: genre['id'], status: true)
        end
      end
    end
  end
end
