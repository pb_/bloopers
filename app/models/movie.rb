class Movie < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :movie_type
  has_many :movie_genres, dependent: :destroy
  has_many :genres, through: :movie_genres
  has_many :movie_actors, dependent: :destroy
  has_many :actors, through: :movie_actors
  has_many :bloopers

  validates :tmdb_id, :name, :description, :image, :movie_type_id, :genres, presence: true
  validates :tmdb_id, numericality: { only_integer: true }
  validates :name, :image, :tmdb_id, :imdb_id, length: { maximum: 256 }

  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :name, analyzer: 'english'
      indexes :description, analyzer: 'english'
    end
  end

  def self.search(query)
    __elasticsearch__.search(
      {
        query: {
          multi_match: {
            query: query,
            fields: ['name^10', 'description']
          }
        }
      }
    )
  end

  def as_indexed_json(options = {})
    self.as_json({
         only: [:name, :description, :image, :slug],
         include: {
             movie_type: { only: :name }
         }
     })
  end
end
