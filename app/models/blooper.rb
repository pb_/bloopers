class Blooper < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :movie

  validates :name, :movie_id, :video_link, presence: true
  validates :name, :video_link, length: { maximum: 256 }

end
