module MoviesHelper
  def cache_key_for_movies
    count          = Movie.where(status: :true).count
    max_updated_at = Movie.where(status: :true).maximum(:updated_at).try(:utc).try(:to_s, :number)
    "movies/all-#{count}-#{max_updated_at}-#{params[:page]}"
  end
end
