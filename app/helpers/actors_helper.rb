module ActorsHelper
  def cache_key_for_actors
    count          = Actor.where(status: :true).count
    max_updated_at = Actor.where(status: :true).maximum(:updated_at).try(:utc).try(:to_s, :number)
    "actors/all-#{count}-#{max_updated_at}-#{params[:page]}"
  end
end
