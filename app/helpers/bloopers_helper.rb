module BloopersHelper
  def cache_key_for_bloopers
    count          = Blooper.where(status: :true).count
    max_updated_at = Blooper.where(status: :true).maximum(:updated_at).try(:utc).try(:to_s, :number)
    "bloopers/all-#{count}-#{max_updated_at}-#{params[:page]}"
  end
end
