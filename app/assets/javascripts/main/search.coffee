# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  $('.search').on 'submit', 'form', (e) ->
    e.preventDefault();

  $(document).on 'keyup', '#search', (e) ->
    e.preventDefault()
    delay(->
      searcher()
    ,500)

  searcher = ->
    search = $('#search').val()
    console.log(search)
    if search.length > 3
      $.ajax
        url: '/search.json',
        type: 'POST'
        data: {q: search}
        dataType: 'json'
        beforeSend: ->
          $('#search-preloader').show()
          $('#results').show()
        success: (data) ->
          results_div = ''
          $.each data, (k,v)->
            if v._index == 'actors'
              link = '/actors/'
            else
              if v._source.movie_type.name == 'TV'
                link = '/tvs/'
              else
                link = '/movies/'

            results_div += '<div class="search-card"><a href="' + link + v._source.slug + '">' +
                '<img src="' + v._source.image + '" width="50px">' +
                '<span>' + v._source.name + '</span>' +
                '</a></div>'
          $('#results-data').html(results_div)
        complete: ->
          $('#search-preloader').hide()
    else
      $('#results').hide()

$(document).ready(ready)
$(document).on('page:load', ready)