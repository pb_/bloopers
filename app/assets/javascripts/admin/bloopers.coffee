ready = ->
  $('#blooper').on 'click', '#show-video', (e)->
    e.preventDefault()
    id = $('#blooper_video_link').val()
    link = '//www.youtube.com/embed/' + id + '?rel=0'
    $('.blooper-video').slideToggle 'slow', ->
      if( $('.blooper-video').is(':hidden') )
        $('#show-video').html('Show')
      else
        $('#show-video').html('Hide')
        $('#blooper-video-iframe').attr('src', link)


$(document).ready(ready)
$(document).on('page:load', ready)