ready = ->
  movie_data = ''
  $('select').material_select()
  $('.datepicker').pickadate ->
    selectMonths: true,
    selectYears: 15
  $('ul.tabs').tabs()

  # search for movie
  $('#movies').on 'click', '#btn-search', (e) ->
    e.preventDefault()
    searcher()

  # search for movie
  $('#movies').on 'keydown', '#search', (e) ->
    if(e.which == 13)
      searcher()

  # copy data
  $('#movies').on 'click', '#copy-data', (e) ->
    e.preventDefault()
    console.log(movie_data)
    $('#movie_name').val(movie_data.title||=movie_data.name)
    $('#movie_name').parent().find('label').addClass('active')
    $('#movie_image').val(movie_data.full_poster_path)
    $('#movie_image').parent().find('label').addClass('active')
    $('#movie_tmdb_id').val(movie_data.id)
    $('#movie_tmdb_id').parent().find('label').addClass('active')
    $('#movie_imdb_id').val(movie_data.imdb_id)
    $('#movie_imdb_id').parent().find('label').addClass('active')
    $('#movie_description').val(movie_data.overview)
    $('#movie_description').parent().find('label').addClass('active')
    $('#movie_release_date').val(movie_data.release_date||=movie_data.first_air_date)
    $('#movie_release_date').parent().find('label').addClass('active')
    $('#movie_vote_average').val(movie_data.vote_average)
    $('#movie_vote_average').parent().find('label').addClass('active')
    $('.lever').trigger('click')
    $.each movie_data.genres, (k,v)->
      genre = $('label:contains("' + v.name + '")')
      $('#' + genre.attr('for')).prop( "checked", true )

  # check actor or add and check actor
  $('#movies').on 'click', '.actor-panel', (e) ->
    id = $(this).data('id')
    name = $(this).data('name')
    console.log(name)
    actor = $('label:contains("' + name + '")')
    if(actor.length == 1)
      $('#' + actor.attr('for')).prop( "checked", true )
    else
      console.log('add actor')
      $.ajax
        url: '/admin/tmdb/add_person.json',
        type: 'POST'
        data: {id: id}
        dataType: 'json'
        beforeSend: ->
          $('#movie-details').hide()
          $('#spinner').show()
        error: (jqXHR) ->
          console.error(jqXHR.statusText)
          Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;' + jqXHR.statusText, 4000, 'red darken-1');
        success: (data) ->
          console.log(data)
          new_actor = '<input checked="checked" type="checkbox" value="' + data.id + '" name="movie[actor_ids][]" id="movie_actor_ids_' + data.id + '">' +
                      '<label for="movie_actor_ids_' + data.id + '">' + data.name + '</label>'
          $('#actors-list').append(new_actor)
        complete: ->
          $('#spinner').hide()
          $('#movie-details').show()

  # select movie
  $('#movies').on 'click', '.movie-panel', (e) ->
    id = $(this).data('id')
    type = $(this).data('type')

    if type == 'tv'
      url = '/admin/tmdb/tv.json'
    else
      url = '/admin/tmdb/movie.json'

    $.ajax
      url: url,
      type: 'POST'
      data: {id: id}
      dataType: 'json'
      beforeSend: ->
        $('#result-tabs').hide()
        $('#spinner').show()
      error: (jqXHR) ->
        console.error(jqXHR.statusText)
        Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;' + jqXHR.statusText, 4000, 'red darken-1');
      success: (data) ->
        console.log(data)
        movie_data = data
        $('#movie-details-image').attr('src', data.full_poster_path)
        $('#movie-details .card-title').text(data.title||=data.name)
        $('#movie-details .card-content p').text(data.overview)
        div_body = ''
        $.each data.persons, (k,v)->
          if v.name != undefined
            if v.profile_path != null
              image = data.image_link + v.profile_path
            else
              image = '/assets/default_profile_picture.png'
            div_body += '<div class="col s12 m9">' +
                '<div data-id="'+v.id+'" data-name="'+v.name+'" class="actor-panel card-panel grey lighten-5 z-depth-1 pointer">' +
                '<div class="row valign-wrapper">' +
                '<div class="col s4">' +
                '<img src="'+image+'" alt="" class="circle responsive-img">' +
                '</div>' +
                '<div class="col s8">' +
                '<span class="black-text">' +
                v.name +
                '</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
          div_body
        $('#movie-details .results').html(div_body);
      complete: ->
        $('#spinner').hide()
        $('#movie-details').show()
        #$('#result-tabs').show()

  # search for movie
  searcher = ->
    search = $('#search').val()
    if search.length > 0 and $('#spinner').is(':hidden')
      $.ajax
        url: '/admin/tmdb/movie_search.json',
        type: 'POST'
        data: {name: search}
        dataType: 'json'
        beforeSend: ->
          $('#result-tabs').hide()
          $('#movie-details').hide()
          $('#tv-tab .results').html('')
          $('#movies-tab .results').html('')
          $('#spinner').show()
        error: (jqXHR) ->
          console.error(jqXHR.statusText)
          Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;' + jqXHR.statusText, 4000, 'red darken-1');
        success: (data) ->
          if data.length <= 1
            Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;No results', 4000, 'red darken-1');
          else
            $("#movies-tab .results").append(render_panel(data.movies, data.image_link, 'movie'));
            $("#tv-tab .results").append(render_panel(data.tv, data.image_link, 'tv'));
            $('#result-tabs').show()
            $('ul.tabs').tabs('select_tab', 'movies-tab')


        complete: ->
          $('#spinner').hide();

render_panel = (data, image_link, type)->
  div_body = ''
  $.each data, (k,v)->
    if v.title != undefined or v.name != undefined
      if v.poster_path != null
        image = image_link + v.poster_path
      else
        image = '/assets/default_profile_picture.png'
      div_body += '<div class="col s12 m9 offset-m3">' +
          '<div data-id="'+v.id+'" data-type="'+type+'" class="movie-panel card-panel grey lighten-5 z-depth-1 pointer">' +
          '<div class="row valign-wrapper">' +
          '<div class="col s4">' +
          '<img src="'+image+'" alt="" class="responsive-img">' +
          '</div>' +
          '<div class="col s8">' +
          '<span class="black-text">' +
          (v.title||=v.name) +
          '</span>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '</div>'
  div_body

$(document).ready(ready)
$(document).on('page:load', ready)