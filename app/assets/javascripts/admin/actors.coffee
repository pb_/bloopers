ready = ->
  $('#actors').on 'click', '#btn-search', (e) ->
    e.preventDefault()
    searcher()

  $('#actors').on 'keydown', '#search', (e) ->
    if(e.which == 13)
      searcher()

  $('#actors').on 'click', '.actor-panel', (e) ->
    id = $(this).data('id')
    $.ajax
      url: '/admin/tmdb/person.json',
      type: 'POST'
      data: {id: id}
      dataType: 'json'
      beforeSend: ->
        $('#results').hide()
        $('#spinner').show()
      error: (jqXHR) ->
        console.error(jqXHR.statusText)
        Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;' + jqXHR.statusText, 4000, 'red darken-1');
      success: (data) ->
        $('#actor_name').val(data.name)
        $('#actor_name').parent().find('label').addClass('active')
        $('#actor_image').val(data.full_profile_path)
        $('#actor_image').parent().find('label').addClass('active')
        $('#actor_tmdb_id').val(data.id)
        $('#actor_tmdb_id').parent().find('label').addClass('active')
        $('#actor_description').val(data.biography)
        $('#actor_description').parent().find('label').addClass('active')
        $('.lever').trigger('click')
      complete: ->
        $('#spinner').hide()
        $('#results').show()

  searcher = ->
    search = $('#search').val()
    if search.length > 0 and $('#spinner').is(':hidden')
      $.ajax
        url: '/admin/tmdb/person_search.json',
        type: 'POST'
        data: {name: search}
        dataType: 'json'
        beforeSend: ->
          $('#results').html('')
          $('#spinner').show()
        error: (jqXHR) ->
          console.error(jqXHR.statusText)
          Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;' + jqXHR.statusText, 4000, 'red darken-1');
        success: (data) ->
          if data.length <= 1
            Materialize.toast('<i class="material-icons">error_outline</i> &nbsp;&nbsp;&nbsp;No results', 4000, 'red darken-1');
          div_body = ''
          last_item = data.pop();
          $.each data, (k,v)->
            if v.name != undefined
              if v.profile_path != null
                image = last_item.image_link + v.profile_path
              else
                image = '/assets/default_profile_picture.png'
              div_body += '<div class="col s12 m9 offset-m3">' +
                  '<div data-id="'+v.id+'" class="actor-panel card-panel grey lighten-5 z-depth-1 pointer">' +
                  '<div class="row valign-wrapper">' +
                  '<div class="col s4">' +
                  '<img src="'+image+'" alt="" class="circle responsive-img">' +
                  '</div>' +
                  '<div class="col s8">' +
                  '<span class="black-text">' +
                  v.name +
                  '</span>' +
                  '</div>' +
                  '</div>' +
                  '</div>' +
                  '</div>'
          $("#results").append(div_body);
        complete: ->
          $('#spinner').hide();

$(document).ready(ready)
$(document).on('page:load', ready)