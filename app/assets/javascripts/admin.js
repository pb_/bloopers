//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require materialize
//= require nprogress
//= require nprogress-turbolinks
//= require_tree ./admin/

NProgress.configure({
    showSpinner: false,
    ease: 'ease',
    speed: 500
});